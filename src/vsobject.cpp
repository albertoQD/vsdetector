#include "vsobject.h"

VSObject::~VSObject(void)
{
}

VSObject::VSObject(void) 
{
}

VSObject::VSObject(const std::string &t_label, const cv::Mat &t_original_image, const cv::Mat &t_mask_image)
{
    m_label = t_label;
    m_image_original = t_original_image.clone();
    m_image_mask = t_mask_image.clone();
    compute_bbox();
}

void VSObject::label(const std::string &t_label)
{
    m_label = t_label;
}

void VSObject::original_image(const cv::Mat &t_image)
{
    m_image_original = t_image.clone();
}

void VSObject::mask_image(const cv::Mat &t_image)
{
    m_image_mask = t_image.clone();
    compute_bbox();
}

void VSObject::bbox_image(const cv::Mat &t_image)
{
    m_image_bbox = t_image.clone();
}

std::string VSObject::label(void) const
{
    return m_label;
}

cv::Mat VSObject::original_image(void) const
{
    return m_image_original;
}

cv::Mat VSObject::mask_image(void) const 
{
    return m_image_mask;
}

cv::Mat VSObject::bbox_image(void) const 
{
    return m_image_bbox;
}

void VSObject::compute_bbox(void)
{
    int min_x = INT_MAX, min_y = INT_MAX;
    int max_x = INT_MIN, max_y = INT_MIN;

    for (int r=0; r<m_image_mask.rows; r+=1)
    {
        for (int c=0; c<m_image_mask.cols; c+=1)
        {
            if (m_image_mask.at<cv::Vec3b>(r,c)[0] == 255)
            {
                min_x = std::min(min_x, c);
                min_y = std::min(min_y, r);
                max_x = std::max(max_x, c);
                max_y = std::max(max_y, r);
            }
        }
    }

    cv::Scalar color(0,255,0);

    m_image_bbox = cv::Mat::zeros(m_image_mask.rows, m_image_mask.cols, CV_8UC3);
    cv::line(m_image_bbox, cv::Point(min_x, min_y), cv::Point(max_x, min_y), color, 3);
    cv::line(m_image_bbox, cv::Point(min_x, min_y), cv::Point(min_x, max_y), color, 3);
    cv::line(m_image_bbox, cv::Point(min_x, max_y), cv::Point(max_x, max_y), color, 3);
    cv::line(m_image_bbox, cv::Point(max_x, min_y), cv::Point(max_x, max_y), color, 3);
}
