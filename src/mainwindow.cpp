#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QFileDialog>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QMessageBox>
#include <QProgressDialog>
#include <QEventLoop>
#include "mattoqimage.hpp"
#include "vsobject.h"
#include "qthreaddetector.h"

#define LABEL_HEIGHT 418
#define LABEL_WIDTH 498

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_init(false)
{
    ui->setupUi(this);

    m_root = new QTreeWidgetItem(ui->treeWidget);
    m_root->setText(0, "Detected Objects");

    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(on_update_progress()));

    statusBar()->showMessage(tr("Loading model ..."));
    init_model();
}

MainWindow::~MainWindow()
{
    delete m_detector;
    delete m_root;
    delete ui;
}

void MainWindow::init_model(void)
{
    std::string file_proto = "../resources/TVG_CRFRNN_new_deploy.prototxt";
    std::string file_model = "../resources/TVG_CRFRNN_COCO_VOC.caffemodel";
    m_detector = new ObjectsDetector(file_proto, file_model);
    m_init = true;
    statusBar()->showMessage(tr("Loading model ... Done"));
}

void MainWindow::prepare_for_viewing(cv::Mat &t_image_io)
{
     // resize accordingly
    if (t_image_io.rows > LABEL_HEIGHT)
    {
        double ratio = (LABEL_HEIGHT-20)/double(t_image_io.rows);
        int new_width = std::floor(t_image_io.cols*ratio);
        cv::resize(t_image_io, t_image_io,
                   cv::Size(new_width, LABEL_HEIGHT),
                   0, 0, cv::INTER_CUBIC);
    }
    else if (t_image_io.cols > LABEL_WIDTH)
    {
        double ratio = (LABEL_WIDTH-20)/double(t_image_io.cols);
        int new_height = std::floor(t_image_io.rows*ratio);
        cv::resize(t_image_io, t_image_io,
                   cv::Size(LABEL_WIDTH, new_height),
                   0, 0, cv::INTER_CUBIC);
    }

    // now pad for better viewing
    int pad_height = std::max(LABEL_HEIGHT-t_image_io.rows, 0);
    int pad_width = std::max(LABEL_WIDTH-t_image_io.cols, 0);
    cv::copyMakeBorder(t_image_io, t_image_io,
                       std::floor(pad_height/2), std::floor(pad_height/2),
                       std::floor(pad_width/2), std::floor(pad_width/2),
                       cv::BORDER_CONSTANT, cv::Scalar(0));
}

void MainWindow::on_update_progress(void)
{
    int tmp = m_progress_dialog->value();
    if (tmp < 95)
    {
        m_progress_dialog->setValue(tmp + 1);
        m_timer.start(2500);
    }
}

void MainWindow::on_actionLoad_image_triggered()
{
    if (! m_init)
    {
        statusBar()->showMessage(tr("The model is not loaded yet. An error maybe?"));
        return;
    }
    
    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Select image to open"),
                QDir::toNativeSeparators(QDir::homePath()),
                tr("Images (*.jpeg *.jpg *.png"));
    if (!filename.isEmpty())
    {
        m_progress_dialog = new QProgressDialog("Please wait while the image is processed ...", "", 0, 100);
        m_progress_dialog->setModal(true);
        m_progress_dialog->setCancelButton(0);

        m_progress_dialog->setValue(0);

        // clean the treeview
        for (auto child : m_root->takeChildren())
        {
            m_root->removeChild(child);
        }

        m_progress_dialog->setValue(1);

        QThreadDetector threaded_detector(m_detector, filename.toStdString(), true);
        QEventLoop eventLoop;

        QObject::connect(&threaded_detector, SIGNAL(finished()), &eventLoop, SLOT(quit()));
        QObject::connect(&threaded_detector, SIGNAL(finished()), &m_timer, SLOT(stop()));

        m_timer.start(2500);
        threaded_detector.start();
        eventLoop.exec();

        m_progress_dialog->setValue(95);

        m_detected_objects = m_detector->get_detected();

        for (auto object : m_detected_objects)
        {
            QTreeWidgetItem * tmp_child = new QTreeWidgetItem(m_root);
            tmp_child->setText(0, QString::fromStdString(object.m_object.label()));

            QTreeWidgetItem * score = new QTreeWidgetItem(tmp_child);
            score->setText(0, QString("Score: %1").arg(object.m_score));

            // QTreeWidgetItem * importance = new QTreeWidgetItem(tmp_child);
            // importance->setText(0, QString("Importance: High"));
        }

        m_progress_dialog->setValue(97);

        // open image
        cv::Mat image = cv::imread(filename.toStdString(), CV_LOAD_IMAGE_COLOR);
        m_current_image = image.clone();
        prepare_for_viewing(image);
        ui->mainImage->setPixmap(QPixmap::fromImage(MatToQImage(image)));

        m_progress_dialog->setValue(100);
        delete m_progress_dialog;
    }
}

void MainWindow::on_actionLoad_video_triggered()
{
    if (! m_init)
    {
        statusBar()->showMessage(tr("The model is not loaded yet. An error maybe?"));
        return;
    }

    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Select video to open"),
                QDir::toNativeSeparators(QDir::homePath()),
                tr("Videos (*.avi)"));
    if (!filename.isEmpty())
    {
        m_progress_dialog = new QProgressDialog("Please wait while the video is processed ...", "", 0, 100);
        m_progress_dialog->setModal(true);
        m_progress_dialog->setCancelButton(0);

        m_progress_dialog->setValue(0);

        // clean the treeview
        for (auto child : m_root->takeChildren())
        {
            m_root->removeChild(child);
        }

        m_progress_dialog->setValue(1);

        QThreadDetector threaded_detector(m_detector, filename.toStdString(), false);
        QEventLoop eventLoop;

        QObject::connect(&threaded_detector, SIGNAL(finished()), &eventLoop, SLOT(quit()));
        QObject::connect(&threaded_detector, SIGNAL(finished()), &m_timer, SLOT(stop()));

        m_timer.start(2500);
        threaded_detector.start();
        eventLoop.exec();

        m_progress_dialog->setValue(95);

        m_detected_objects = m_detector->get_detected();

        for (auto object : m_detected_objects)
        {
            QTreeWidgetItem * tmp_child = new QTreeWidgetItem(m_root);
            tmp_child->setText(0, QString::fromStdString(object.m_object.label()));

            QTreeWidgetItem * score = new QTreeWidgetItem(tmp_child);
            score->setText(0, QString("Score: %1").arg(object.m_score));

            // QTreeWidgetItem * importance = new QTreeWidgetItem(tmp_child);
            // importance->setText(0, QString("Importance: High"));
        }

        m_progress_dialog->setValue(97);

        // TODO: open first frame

        m_progress_dialog->setValue(100);
        delete m_progress_dialog;
    }
}



void MainWindow::on_actionShow_detected_mask_triggered()
{

}

void MainWindow::on_actionList_of_objects_triggered()
{
    QMessageBox::information(this,"List objects",QString("List of possible objects to detect: \n\n"
                                                  "  - Person\n"
                                                  "  - Animan : {bird, cat, cow, horse, sheep}\n"
                                                  "  - Vehicle: {aeroplane, bicycle, boat, bus, car, motorbike, train}\n"
                                                  "  - Indoor: {bottle, chair, dining table, potted plant, sofa, tv/monitor"));
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this,"About",QString("Developed by Alberto Quintero-Delgado \nEmail: ajquinterod@gmail.com\n\n "
                                                  "Using models from http://www.robots.ox.ac.uk/~szheng/CRFasRNN.html"));

}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if (m_root->isSelected() || item->parent() != m_root)
    {
        return;
    }

    int idx = m_root->indexOfChild(item);
    cv::Mat object_mask;
    cv::Mat original_image = m_detected_objects[idx].m_object.original_image();
    if (ui->actionShow_detected_mask->isChecked())
    {
        object_mask = m_detected_objects[idx].m_object.mask_image();
    }
    else
    {
        object_mask = m_detected_objects[idx].m_object.bbox_image();
    }

    cv::Mat tmp_image = original_image + object_mask;
    prepare_for_viewing(tmp_image);
    ui->mainImage->setPixmap(QPixmap::fromImage(MatToQImage(tmp_image)));
}
