#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "detector.h"

class QTreeWidgetItem;
class QTimer;
class QProgressDialog;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_actionLoad_image_triggered();
    void on_actionLoad_video_triggered();
    void on_actionShow_detected_mask_triggered();
    void on_actionList_of_objects_triggered();
    void on_actionAbout_triggered();
    void on_actionQuit_triggered();

    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_update_progress(void);

private:
    void prepare_for_viewing(cv::Mat &t_image_io);
    void init_model(void);

    Ui::MainWindow *ui;
    QTreeWidgetItem * m_root;
    QTimer m_timer;
    QProgressDialog * m_progress_dialog;
    ObjectsDetector * m_detector;
    cv::Mat m_current_image;
    std::vector<DetectedObject> m_detected_objects;
    bool m_init;
};

#endif // MAINWINDOW_H
