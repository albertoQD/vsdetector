#ifndef OBJECTSDETECTOR_HPP
#define OBJECTSDETECTOR_HPP

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include "caffe/caffe.hpp"
#include "caffe/util/io.hpp"
#include "caffe/blob.hpp"

#include "vsobject.h"

using namespace caffe;


struct DetectedObject 
{
    VSObject m_object;
    float m_score;
    int m_class_id;

    DetectedObject(void) : m_score(-1) 
    { 
    }
    
    DetectedObject(const VSObject &t_vsobject, float t_score, int t_class)
    {
        m_object = t_vsobject;
        m_score = t_score;
        m_class_id = t_class;
    }
};

class ObjectsDetector
{
public:
    ObjectsDetector(const std::string &t_protofile, const std::string &t_modelfile);
    ~ObjectsDetector(void);

    bool process_file(const std::string &t_filename, bool t_single_image);
    bool process_image(const std::string &t_imagefile);
    bool process_video(const std::string &t_videofile);
    void max_dimension(int t_max_dim);
    void general_threshold(int t_threshold);

    std::vector<DetectedObject> get_detected(void);

    void print_detected(void);

    void save_detected(const std::string &t_prefix);

private:
    // Methods
    bool apply_transformation(void);
    void process_blob(caffe::Blob<float>* t_blob_in);

    // Props
    int m_max_dim;
    int m_general_threshold;
    caffe::Net<float> m_lenet;
    time_t m_tstart, m_tend;
    cv::Mat m_image_original;
    cv::Size m_size_original;
    cv::Mat m_image_resized;
    int m_resized_width;
    int m_resized_height;
    cv::Mat m_image_padded;
    cv::Size m_size_padded;
    std::vector<float> m_blob_padded;
    std::vector<DetectedObject> m_detected_objs;
    std::vector<std::string> m_classes = 
    {
        "background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "dining_table", "dog", "horse", "motorbike", "person", "potted_plant", "sheep", "sofa", "train", "tv/monitor"
    };
};


#endif