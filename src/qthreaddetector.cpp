#include "qthreaddetector.h"

QThreadDetector::QThreadDetector(ObjectsDetector * t_detector,
                                 const std::string &t_filename,
                                 bool t_single_image)
{
    m_detector = t_detector;
    m_filename = t_filename;
    m_single_image = t_single_image;
}

void QThreadDetector::run()
{
    m_detector->process_file(m_filename, m_single_image);
}
