#include "mainwindow.h"
#include <QApplication>

#include "caffe/caffe.hpp"
#include "caffe/util/io.hpp"
#include "caffe/blob.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    google::InitGoogleLogging("XXX");
    google::SetCommandLineOption("GLOG_minloglevel", "3");

    MainWindow w;
    w.show();

    return a.exec();
}
