#include "detector.h"

using namespace caffe;

ObjectsDetector::ObjectsDetector(const std::string &t_protofile, const std::string &t_modelfile) :
    m_lenet(t_protofile, caffe::TEST),
    m_max_dim(180), // 250 gives good results but slow, after I set for 150
    m_general_threshold(9)
{
    // Change this to accept also to use GPU
    Caffe::set_mode(Caffe::CPU);
    m_lenet.CopyTrainedLayersFrom(t_modelfile);
}

ObjectsDetector::~ObjectsDetector(void)
{
}


std::vector<DetectedObject> ObjectsDetector::get_detected(void)
{
    // sort by score and remove empty
    auto sort_desc_score = [&](const DetectedObject &t_a, const DetectedObject &t_b)
    {
        return t_a.m_score > t_b.m_score;
    };
    std::sort(begin(m_detected_objs), end(m_detected_objs), sort_desc_score);
    std::vector<DetectedObject> result;
    for (auto detected : m_detected_objs)
    {
        if (detected.m_score > -1)
        {
            result.push_back(detected);
        }
    }
    return result;
}

void ObjectsDetector::print_detected(void)
{
    for (auto detected : m_detected_objs)
    {
        std::cout << "{ " << std::endl;
        std::cout << "\t label : " << detected.m_object.label() << std::endl;
        std::cout << "\t score : " << detected.m_score << std::endl;
        std::cout << "\t class_id : " << detected.m_class_id << std::endl;
        std::cout << "}" << std::endl;
    }
}


void ObjectsDetector::save_detected(const std::string &t_prefix)
{
    for (auto detected : m_detected_objs)
    {
        cv::imwrite(t_prefix+"-"+detected.m_object.label()+".png", detected.m_object.mask_image());
    }
}

void ObjectsDetector::general_threshold(int t_threshold)
{
    m_general_threshold = t_threshold;
}

bool ObjectsDetector::process_file(const std::string &t_filename, bool t_single_image)
{
    if (t_single_image)
    {
        return process_image(t_filename);
    }
    else
    {
        return process_video(t_filename);
    }
}

bool ObjectsDetector::process_video(const std::string &t_videofile)
{
    // read video
    cv::VideoCapture cap(t_videofile);
    if (!cap.isOpened())
    {
        std::cerr << " :: ERROR - Could not open the file: " << t_videofile << std::endl;
        return false;
    }

    // clean previous results
    m_detected_objs.clear();
    for (size_t i=0; i<m_classes.size(); i+=1)
    {
        m_detected_objs.push_back(DetectedObject());
    }

    cv::Mat frame_image;
    // get FPS to know how many frames to drop
    int video_fps = (int) cap.get(CV_CAP_PROP_FPS);
    video_fps = (video_fps == 0) ? 30 : video_fps;

    int frames_count = (int) cap.get(CV_CAP_PROP_FRAME_COUNT);

    int current_frame = 0;
    cap.set(CV_CAP_PROP_POS_FRAMES, (double) current_frame);

    while(1) 
    {
        cap >> m_image_original;
        if (m_image_original.empty()) break;

        m_size_original = m_image_original.size();

        // resize to faster processing and less memory use
        if (m_size_original.height > m_size_original.width)
        {
            if (m_size_original.height > m_max_dim)
            {
                double ratio = m_max_dim/double(m_image_original.rows);
                int new_width = int(m_image_original.cols*ratio);
                cv::resize(m_image_original, m_image_resized, cv::Size(new_width, m_max_dim), 0, 0, cv::INTER_CUBIC);
            }
        }
        else
        {
            if (m_size_original.width > m_max_dim)
            {
                double ratio = m_max_dim/double(m_image_original.cols);
                int new_height = int(m_image_original.rows*ratio);
                cv::resize(m_image_original, m_image_resized, cv::Size(m_max_dim, new_height), 0, 0, cv::INTER_CUBIC);
            }
        }

        if (!apply_transformation())
        {
            std::cerr << " :: ERROR - Something wrong applying transformation to image" << std::endl;
            return false;
        }

        time(&m_tstart);
        std::vector<int> _shape = {1, m_image_padded.channels(), m_size_padded.height, m_size_padded.width};
        caffe::Blob<float> * data_in = new caffe::Blob<float>(_shape);
        data_in->set_cpu_data(m_blob_padded.data());
        auto output = m_lenet.Forward({data_in});
        time(&m_tend);

        std::cout << " :: Frame " << current_frame << "/" << frames_count << " took " << difftime(m_tend, m_tstart) << " seconds" << std::endl;

        process_blob(output[0]);

        delete data_in;

        current_frame += video_fps;
        cap.set(CV_CAP_PROP_POS_FRAMES, (double) current_frame);
    }

    cap.release();
    return true;
}

bool ObjectsDetector::process_image(const std::string &t_imagefile)
{
    // read image
    m_image_original = cv::imread(t_imagefile, CV_LOAD_IMAGE_COLOR);
    m_size_original = m_image_original.size();

    // clean previous results
    m_detected_objs.clear();
    for (size_t i=0; i<m_classes.size(); i+=1)
    {
        m_detected_objs.push_back(DetectedObject());
    }

    // resize to faster processing and less memory use
    if (m_size_original.height > m_size_original.width)
    {
        if (m_size_original.height > m_max_dim)
        {
            double ratio = m_max_dim/double(m_image_original.rows);
            int new_width = int(m_image_original.cols*ratio);
            cv::resize(m_image_original, m_image_resized, cv::Size(new_width, m_max_dim), 0, 0, cv::INTER_CUBIC);
        }
    }
    else
    {
        if (m_size_original.width > m_max_dim)
        {
            double ratio = m_max_dim/double(m_image_original.cols);
            int new_height = int(m_image_original.rows*ratio);
            cv::resize(m_image_original, m_image_resized, cv::Size(m_max_dim, new_height), 0, 0, cv::INTER_CUBIC);
        }
    }

    if (!apply_transformation())
    {
        std::cerr << " :: ERROR - Something wrong applying transformation to image" << std::endl;
        return false;
    }

    time(&m_tstart);
    std::vector<int> _shape = {1, m_image_padded.channels(), m_size_padded.height, m_size_padded.width};
    caffe::Blob<float> * data_in = new caffe::Blob<float>(_shape);
    data_in->set_cpu_data(m_blob_padded.data());
    auto output = m_lenet.Forward({data_in});
    time(&m_tend);

    std::cout << " :: Going through the network took " << difftime(m_tend, m_tstart) << " seconds" << std::endl;

    process_blob(output[0]);

    delete data_in;

    return true;
}

void ObjectsDetector::process_blob(caffe::Blob<float>* t_blob_in)
{
    std::vector<int> blob_shape = t_blob_in->shape();
    int height = blob_shape[2];
    int width = blob_shape[3];
    int n_images = blob_shape[1]; // for predictions

    // search for objects on the image and its max_values 
    std::vector<std::pair<int,float>> max_values; // {class, max_value}
    for (int c=1; c<n_images; c+=1)
    {
        float class_max_value = FLT_MIN;
        for (int h=0; h<height; h+=1)
        {
            for (int w=0; w<width; w+=1)
            {
                float tmp_value = t_blob_in->data_at(0,c,h,w);
                class_max_value = std::max(tmp_value, class_max_value);
            }
        }

        // play with this threshold
        if (class_max_value > m_general_threshold)
        {
            max_values.push_back(make_pair(c, class_max_value));
        }
    }

    // extract background image
    cv::Mat bg_image = cv::Mat::zeros(height, width, CV_32FC1);
    for (int h=0; h<height; h+=1)
    {
        for (int w=0; w<width; w+=1)
        {
            bg_image.at<float>(h,w) = t_blob_in->data_at(0,0,h,w);
        }
    }
    // extract images for the top classes
    for (auto detected : max_values)
    {
        // if the detected object has bigger score than the previous we process 
        if (detected.second <= m_detected_objs[detected.first].m_score)
        {
            continue;
        }

        cv::Mat tmp_image = cv::Mat::zeros(height, width, CV_32FC1);
        for (int h=0; h<height; h+=1)
        {
            for (int w=0; w<width; w+=1)
            {
                tmp_image.at<float>(h,w) = t_blob_in->data_at(0,detected.first,h,w);
            }
        }

        // binarize
        cv::normalize(tmp_image-bg_image, tmp_image, 0, 255, cv::NORM_MINMAX);
        cv::threshold(tmp_image, tmp_image, 200, 255, cv::THRESH_BINARY);
        cv::Mat final_image = tmp_image(cv::Rect(0,0,m_image_resized.cols,m_image_resized.rows));

        // take back to original size
        cv::resize(final_image, final_image, m_size_original, 0, 0, cv::INTER_CUBIC);

        final_image.convertTo(final_image, CV_8UC1);
        cv::merge({final_image, final_image, final_image}, final_image);


        m_detected_objs[detected.first] = DetectedObject(VSObject(m_classes[detected.first], m_image_original, final_image), detected.second, detected.first);
    }
}

void ObjectsDetector::max_dimension(int t_max_dim)
{
    m_max_dim = t_max_dim;
}

bool ObjectsDetector::apply_transformation(void)
{
    m_image_padded = m_image_resized.clone();
    int img_height_orig = m_image_padded.rows;
    int img_width_orig = m_image_padded.cols;

    m_image_padded.convertTo(m_image_padded, CV_32F);

    for (int r=0; r<img_height_orig; r+=1)
    {
        for (int c=0; c<img_width_orig; c+=1)
        {
            m_image_padded.at<cv::Vec3f>(r,c)[0] -= 103.939;
            m_image_padded.at<cv::Vec3f>(r,c)[1] -= 116.779;
            m_image_padded.at<cv::Vec3f>(r,c)[2] -= 123.68;
        }
    }

    int pad_height = std::max(m_max_dim - img_height_orig, 0);
    int pad_width = std::max(m_max_dim - img_width_orig, 0);

    cv::copyMakeBorder(m_image_padded, m_image_padded, 0, pad_height, 0, pad_width, cv::BORDER_CONSTANT, cv::Scalar(0));

    int padded_height = m_image_padded.rows;
    int padded_width = m_image_padded.cols;

    if (!m_image_padded.isContinuous())
    {
        std::cerr << " :: ERROR - Image data is not continuous" << std::endl;;
        return false;
    }

    m_blob_padded.resize(m_image_padded.channels()*padded_height*padded_width);
    int top_index=0;
    for (int h=0; h<padded_height; h+=1)
    {
        const float* ptr = m_image_padded.ptr<float>(h);
        int img_index=0;
        for (int w=0; w<padded_width; w+=1)
        {
            for (int c=0; c<m_image_padded.channels(); c+=1)
            {
                top_index = (c * padded_height + h) * padded_width + w;
                float pixel = static_cast<float>(ptr[img_index++]);
                m_blob_padded[top_index] = pixel;
            }
        }
    }

    m_size_padded = m_image_padded.size();
    return true;
}
