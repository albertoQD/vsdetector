#ifndef QTHREADDETECTOR_H
#define QTHREADDETECTOR_H

#include <QThread>
#include "detector.h"

class QThreadDetector : public QThread
{
    Q_OBJECT

public:
    QThreadDetector(ObjectsDetector * t_detector,
                    const std::string &t_file,
                    bool t_single_image);

    void run();

private:
    ObjectsDetector * m_detector;
    std::string m_filename;
    bool m_single_image;
};

#endif // QTHREADDETECTOR_H
