#ifndef VSOBJECT_HPP
#define VSOBJECT_HPP

#include <string>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

class VSObject
{
public:
    VSObject(void);
    VSObject(const std::string &t_label, const cv::Mat &t_original_image, const cv::Mat &t_mask_image);
    ~VSObject(void);

    void label(const std::string &t_label);
    void mask_image(const cv::Mat &t_mask_image);
    void bbox_image(const cv::Mat &t_bbox_image);
    void original_image(const cv::Mat &t_original_image);

    std::string label(void) const;
    cv::Mat mask_image(void) const; 
    cv::Mat bbox_image(void) const;
    cv::Mat original_image(void) const;

private: 
    void compute_bbox(void);

    std::string m_label;
    cv::Mat m_image_mask;
    cv::Mat m_image_bbox;
    cv::Mat m_image_original;
};


#endif